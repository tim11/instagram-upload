<?php

namespace Jlabs\Tools\Social;


use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;

class Instagram
{
    private $userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36";
    private $url = "https://www.instagram.com";
    /**
     * @var string $login Логин пользователя
     */
    private $login;
    /**
     * @var string $password Пароль пользователя
     */
    private $password;

    /**
     * @var string|null Последняя ошибка
     */
    private $lastError = null;

    /**
     * @var Client Соединение
     */
    private $connect;

    /**
     * @var boolean Есть ли авторизация
     */
    private $isAuth = false;

    /**
     * @var integer|null ID клиента
     */
    private $userId = null;

    /**
     * @var CookieJar Куки
     */
    private $cookie;

    /**
     * Instagram constructor.
     * @param string $login Логин пользователя
     * @param string $password Пароль пользователя
     */
    public function __construct($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    public function connect(){
        try {
            $this->cookie = new CookieJar();
            $this->connect = new Client([
                "base_uri" => $this->url,
                "http_errors" => false,
                "headers" => [
                    "User-Agent" => $this->userAgent
                ],
                "cookies" => $this->cookie,
                "verify" => false,
            ]);
            $response = $this->connect->head("/");
            if ($response->getStatusCode() != 200){
                $this->lastError = $response->getStatusCode();
            }else{
                $cookies = [];
                foreach ($this->cookie->toArray() as $cookie){
                    $cookies[$cookie["Name"]] = $cookie["Value"];
                }
                if (!isset($cookies["csrftoken"])){
                    $this->lastError = "Не определен CSRF токен";
                }else{
                    $response = $this->connect->post("/accounts/login/ajax/", [
                        "form_params" => [
                            "username" => $this->login,
                            "password" => $this->password,
                            "queryParams" => [],
                            "optIntoOneTap" => false
                        ],
                        "headers" => [
                            "x-ig-app-id" => isset($cookie["ig_did"]) ? $cookie["ig_did"] : "1217981644879628",
                            "x-csrftoken" => $cookies["csrftoken"],
                            "User-Agent" => $this->userAgent
                        ],
                        "cookies" => $this->cookie
                    ]);
                    $json = json_decode($response->getBody()->getContents());
                    if ($json->authenticated == false){
                        $this->lastError = "Неверный логин/пароль";
                    }else{
                        $this->isAuth = true;
                        $this->userId = $json->userId;
                    }
                }
            }
        }catch (\Exception $e){
            $this->lastError = $e->getMessage();
        }
    }

    /**
     * @return bool Есть ли ошибка
     */
    public function hasError(){
        return $this->lastError !== null;
    }

    /**
     * @return string|null Текст послежней ошибки
     */
    public function getLastError(){
        return $this->lastError;
    }

    /**
     * @return boolean Авторизован ли клиент
     */
    public function isAuth(){
        return $this->isAuth;
    }

    /**
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

}